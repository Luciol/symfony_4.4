const link = document.querySelector('.js-like-article');

link.addEventListener('click', (e) => {
    e.preventDefault();
    const target = e.currentTarget;
    target.classList.toggle('fa-heart-o');
    target.classList.toggle('fa-heart');

    fetch(target.getAttribute('href'), {method: 'POST'})
        .then((res) => res.json())
        .then((data) => {
            document.querySelector('.js-like-article-count').innerHTML = data.hearts;
        })
        .catch((err) => console.error(err));
});