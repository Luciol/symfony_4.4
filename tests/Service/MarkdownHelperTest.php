<?php

/*
 * @author Loïc Gallay <lgallay@orange.fr>
 */

namespace App\Tests\Service;

use App\Service\MarkdownHelper;
use Michelf\MarkdownInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Contracts\Cache\ItemInterface;

/**
 * Class MarkdownHelperTest.
 */
class MarkdownHelperTest extends TestCase
{
    public function testParseWithCachedItemReturnCacheItemValue()
    {
        $source = 'Hello World';

        $markdown = $this->createMock(MarkdownInterface::class);

        $cacheItem = $this->createMock(ItemInterface::class);
        $cacheItem
            ->expects($this->any())
            ->method('isHit')
            ->willReturn(true)
        ;

        $cacheItem
            ->expects($this->any())
            ->method('get')
            ->willReturn($source)
        ;

        $cache = $this->createMock(AdapterInterface::class);
        $cache
            ->expects($this->any())
            ->method('getItem')
            ->with($this->equalTo('markdown_'.md5($source)))
            ->willReturn($cacheItem)
        ;

        $helper = new MarkdownHelper($cache, $markdown);
        $result = $helper->parse($source);

        $this->assertEquals($source, $result, 'Should return Hello world');
    }

    public function testParseWithCacheItemNotHitReturnCacheItemValue()
    {
        $source = 'Test';
        $transformedSource = 'test';

        $markdown = $this->createMock(MarkdownInterface::class);
        $markdown
            ->expects($this->atLeastOnce())
            ->method('transform')
            ->with($this->equalTo($source))
            ->willReturn($transformedSource)
        ;

        $cacheItem = $this->createMock(ItemInterface::class);
        $cacheItem
            ->expects($this->any())
            ->method('isHit')
            ->willReturn(false)
        ;
        $cacheItem
            ->expects($this->any())
            ->method('set')
            ->willReturn($cacheItem)
        ;
        $cacheItem
            ->expects($this->any())
            ->method('get')
            ->willReturn($transformedSource)
        ;

        $cache = $this->createMock(AdapterInterface::class);
        $cache
            ->expects($this->any())
            ->method('getItem')
            ->with($this->equalTo('markdown_'.md5($source)))
            ->willReturn($cacheItem)
        ;
        $cache
            ->expects($this->any())
            ->method('save')
            ->willReturn(true)
        ;

        $helper = new MarkdownHelper($cache, $markdown);
        $result = $helper->parse($source);

        $this->assertEquals($transformedSource, $result, 'Should return transformed source');
    }
}
