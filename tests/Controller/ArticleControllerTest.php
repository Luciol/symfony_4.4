<?php

/*
 * @author Loïc Gallay <lgallay@orange.fr>
 */

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ArticleControllerTest.
 */
class ArticleControllerTest extends WebTestCase
{
    /**
     * @var KernelBrowser
     */
    private $client;

    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    /**
     * @dataProvider urlsProvider
     */
    public function testHomepageAndArticlePageReturnHttpOk($url)
    {
        $this->client->request('GET', $url);
        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }

    public function urlsProvider()
    {
        yield 'homepage' => ['/'];
        yield 'article' => ['/news/1'];
    }

    public function testHeartAnArticleReturnHttpOk()
    {
        $this->client->request('POST', '/news/1/heart');
        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }
}
